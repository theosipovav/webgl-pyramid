<?php
include_once 'db.php';

if (isset($_GET['f'])) {
    if ($_GET['f'] == 'ajaxLogout') {
        ajaxLogout();
    }
    if ($_GET['f'] == 'ajaxSetAnswer') {
        ajaxSetAnswer();
    }
    if ($_GET['f'] == 'ajaxRemoveTask') {
        ajaxRemoveTask();
    }
}



/**
 * AJAX. Выход пользователя из системы
 */
function ajaxLogout()
{

    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    session_start();
    $_SESSION = [];
    $res['status'] = 1;
    echo json_encode($res);
    die();
}

/**
 * AJAX. Создание ответа на задачу
 */
function ajaxSetAnswer()
{
    $h = $_GET['h'];
    $a = $_GET['a'];
    $v = $_GET['v'];
    $task = $_GET['task'];
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];
    if (dbSetAnswer($task, $h, $a, $v)) {
        $res['status'] = 1;
    }
    echo json_encode($res);
    die();
}

/**
 * AJAX. Удаление задания через Ajax запрос
 */
function ajaxRemoveTask()
{
    session_start();
    $res = [];
    $res['status'] = 0;
    $res['data'] = [];

    $id = $_GET['id'];

    session_start();
    if ($_SESSION['user']['role'] != 1) {
        $res['status'] = 0;
        $res['data'] = ["Недостаточно прав на совершения данного действия"];
        echo json_encode($res);
        die();
    }
    if (dbRemoveTask($id) == false) {
        $res['status'] = 0;
        $res['data'] = ["Ошибка при выполнение запроса к базе данных"];
        echo json_encode($res);
        die();
    }
    $res['status'] = 1;
    $res['data'] = ["Задание удалено"];
    echo json_encode($res);
    die();
}
