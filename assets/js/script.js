$(document).ready(function () {
  inputRussianOnly("input#inputUsername");

  $("form#FormTask input").change(function (e) {
    webGLStart();
  });

  /**
   * Удаление задания
   */
  $(".btn-remove-task").click(function (e) {
    let modal = $("#modalRemoveTask");
    let id = $(this).attr("data-task-id");
    modal.find("#modalRemoveTaskLabel>span").html(id);
  });

  $("#inputRole").change(function (e) {
    if ($(this).val() == 1) {
      $("#ContainerSelectTeachers").addClass("hidden");
    } else {
      $("#ContainerSelectTeachers").removeClass("hidden");
    }
  });

  /**
   * Удаление задания
   */
  $("#modalRemoveTask .btn-ok").click(function (e) {
    var id = $("#modalRemoveTaskLabel>span").html();
    $(this).attr("disabled", "true");
    let url = "ajax.php?f=ajaxRemoveTask&id=" + id;
    $.ajax({
      dataType: "json",
      url: url,
      success: function (res) {
        // ЗАдание не удалено
        if (res.status == 0) {
          alert("Не удалось удалить задание");
          console.log(res.data);
        }
        // Задание удалено
        if (res.status == 1) {
          location.reload();
        }
      },
      error: function (jqXHR, exception) {
        console.log("Error!");
        console.log(jqXHR);
        console.log(exception);
      },
    });
  });
  var canvas = document.getElementById("CanvasPyramid");
  if (canvas !== null) {
    webGLStart();
  }
});

/**
 * Регистрация пользователя в системе
 */
function reg() {
  var form = $("form#formReg");
  var isValid = true;
  var textValid = "";

  $("form#formReg input").removeClass("is-invalid");
  $("form#formReg input").removeClass("is-invalid");
  $("#cardFormReg").addClass("hidden");
  $("#cardFormReg").removeClass("border-success");
  $("#cardFormReg").removeClass("border-danger");
  $("#cardFormReg .card-body").removeClass("text-success");
  $("#cardFormReg .card-body").removeClass("text-danger");

  var mail = $("#inputRegMail");
  var password = $("#inputRegPassword");
  var repeatPassword = $("#inputRegRepeatPassword");
  var username = $("#inputRegUsername");

  if ($("#inputRegMail").val() == "") {
    isValid = false;
    $("#inputRegMail").addClass("is-invalid");
    textValid += "Поле 'Электронная почта' не заполнено<br>";
  } else {
    $("#inputRegMail").addClass("is-valid");
  }
  if ($("#inputRegPassword").val() == "") {
    isValid = false;
    $("#inputRegPassword").addClass("is-invalid");
    textValid += "Поле 'Пароль' не заполнено<br>";
  } else {
    $("#inputRegPassword").addClass("is-valid");
  }
  if ($("#inputRegRepeatPassword").val() == "") {
    isValid = false;
    $("#inputRegRepeatPassword").addClass("is-invalid");
    textValid += "Поле 'Повторите пароль' не заполнено<br>";
  } else {
    $("#inputRegRepeatPassword").addClass("is-valid");
  }
  if ($("#inputRegUsername").val() == "") {
    isValid = false;
    $("#inputRegUsername").addClass("is-invalid");
    textValid += "Поле 'Отображаемое имя' не заполнено<br>";
  } else {
    $("#inputRegUsername").addClass("is-valid");
  }
  if ($("#inputRegPassword").val() != $("#inputRegRepeatPassword").val()) {
    isValid = false;
    $("#inputRegPassword").addClass("is-invalid");
    $("#inputRegRepeatPassword").addClass("is-invalid");
    textValid += "Введенные пароли не совпадают<br>";
  } else {
    $("#inputRegPassword").addClass("is-valid");
    $("#inputRegRepeatPassword").addClass("is-valid");
  }

  if (isValid == false) {
    $("#cardFormReg").addClass("border-danger");
    $("#cardFormReg .card-body").addClass("text-danger");
    $("#cardFormReg").removeClass("hidden");
    $("#cardFormReg .card-text").html(textValid);
    return false;
  }

  let formSerialize = $(form).serialize();
  $.ajax({
    url: "ajax.php?f=reg",
    type: "POST",
    data: formSerialize,
    dataType: "json",
    success: function (res) {
      // Пользователь с таким почтовым адресом уже зарегистрирован
      if (res.status == 0) {
        $("#cardFormReg").addClass("border-danger");
        $("#cardFormReg .card-body").addClass("text-danger");
        $("#cardFormReg").removeClass("hidden");
        $("#cardFormReg .card-text").html("Пользователь с таким почтовым адресом уже зарегистрирован.");
      }
      // Пользователь зарегистрирован
      if (res.status == 1) {
        $("#cardFormReg").addClass("border-success");
        $("#cardFormReg .card-body").addClass("text-success");
        $("#cardFormReg").removeClass("hidden");
        $("#cardFormReg .card-text").html("Пользователь зарегистрирован!");
        $("#modalReg").modal("hide");
      }
    },
    error: function (jqXHR, exception) {
      console.log("Error!");
      console.log(jqXHR);
      console.log(exception);
    },
  });
}

/**
 * Авторизация пользователя в системе
 */
function login() {
  var form = $("#formLogin");
  var card = $("#cardFormLogin");
  var isValid = true;
  var textValid = "";
  form.find("input").removeClass("is-invalid");
  form.find("input").removeClass("is-valid");
  card.addClass("hidden");
  card.removeClass("border-success");
  card.removeClass("border-danger");
  card.find(".card-body").removeClass("text-success");
  card.find(".card-body").removeClass("text-danger");
  var mail = form.find("input[name=mail]");
  var password = form.find("input[name=password]");
  if (mail.val() == "") {
    isValid = false;
    mail.addClass("is-invalid");
    textValid += "Поле 'Электронная почта' не заполнено<br>";
  } else {
    mail.addClass("is-valid");
  }
  if (password.val() == "") {
    isValid = false;
    password.addClass("is-invalid");
    textValid += "Поле 'Пароль' не заполнено<br>";
  } else {
    password.addClass("is-valid");
  }
  if (isValid == false) {
    card.addClass("border-danger");
    card.find(".card-body").addClass("text-danger");
    card.removeClass("hidden");
    card.find(".card-body").html(textValid);
    return false;
  }
  let formSerialize = $(form).serialize();
  $.ajax({
    url: "ajax.php?f=login",
    type: "POST",
    data: formSerialize,
    dataType: "json",
    success: function (res) {
      // Пользователь не найден.
      if (res.status == 0) {
        card.find(".card-body").addClass("text-danger");
        card.find(".card-text").html("Пользователь не найден. Возможно введен неверный логин или пароль ");
        card.addClass("border-danger");
        card.removeClass("hidden");
      }
      // Пользователь найден.
      if (res.status == 1) {
        card.find(".card-body").addClass("text-success");
        card.find(".card-text").html("Пользователь найден. Возможно введен неверный логин или пароль ");
        card.addClass("border-success");
        card.removeClass("hidden");
        $("#modalLogin").modal("hide");
        location.reload();
      }
    },
    error: function (jqXHR, exception) {
      console.log("Error!");
      console.log(jqXHR);
      console.log(exception);
    },
  });
}

/**
 * Задать вопрос
 */
function setAnswer() {
  let h = parseInt($("#inputPyramidH").val());
  if (!(h > 0)) {
    h = 0;
  }
  let a = parseInt($("#inputPyramidA").val());
  if (!(a > 0)) {
    a = 0;
  }
  let v = parseInt($("#inputPyramidV").val());
  if (!(v > 0)) {
    v = 0;
  }
  if (h == 0 || a == 0 || v == 0) {
    if (h === 0) {
      $("#inputPyramidH").addClass("is-invalid");
    }
    if (a === 0) {
      $("#inputPyramidA").addClass("is-invalid");
    }
    if (v === 0) {
      $("#inputPyramidV").addClass("is-invalid");
    }
    return;
  }

  let taskId = $("#inputTask").val();

  $.ajax({
    url: "ajax.php?f=ajaxSetAnswer&task=" + taskId + "&h=" + h + "&a=" + a + "&v=" + v,
    type: "GET",
    dataType: "json",
    success: function (res) {
      if (res.status == 0) {
        console.log("Ошибка:");
        console.log(res);
      }
      if (res.status == 1) {
        location.reload();
      }
    },
    error: function (jqXHR, exception) {
      console.log("Error!");
      console.log(jqXHR);
      console.log(exception);
    },
  });
}

/**
 * Выход пользователя из системы
 */
function logout() {
  $.ajax({
    url: "ajax.php?f=ajaxLogout",
    type: "POST",
    dataType: "json",
    success: function (res) {
      // Произошла ошибка
      if (res.status == 0) {
        console.log("Ошибка:");
        console.log(res);
      }
      // Пользователь успешно вышел из системы.
      if (res.status == 1) {
        location.reload();
      }
    },
    error: function (jqXHR, exception) {
      console.log("Error!");
      console.log(jqXHR);
      console.log(exception);
    },
  });
}

function validMail(value) {
  var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
  var valid = re.test(value);
  if (valid) output = "Адрес эл. почты введен правильно!";
  else output = "Адрес электронной почты введен неправильно!";
  document.getElementById("message").innerHTML = output;
  return valid;
}
function validPhone(value) {
  var re = /^\d[\d\(\)\ -]{4,14}\d$/;
  var valid = re.test(value);
  if (valid) output = "Номер телефона введен правильно!";
  else output = "Номер телефона введен неправильно!";
  document.getElementById("message").innerHTML = document.getElementById("message").innerHTML + "<br />" + output;
  return valid;
}

/**
 * Ввод только рисских букв в элемент управления
 * @param {*} target
 */
function inputRussianOnly(target) {
  if (document.querySelectorAll(target).length == 0) {
    return;
  }

  let input = document.querySelector(target);

  input.addEventListener("input", () => {
    var re = /[^А-я .]/;
    var result = re.exec(input.value);
    var inputClassList = input.classList;
    if (result == null) {
      if (inputClassList.contains("is-invalid")) {
        inputClassList.remove("is-invalid");
      }
    } else {
      if (!inputClassList.contains("is-invalid")) {
        inputClassList.add("is-invalid");
      }
    }
    input.value = input.value.replace(/[^А-я .]/, "");
  });
}

//
/**
 *
 */
var pyramidH = 1;
var pyramidA = 1;
var pyramidB = 1;

var gl;
var shaderProgram;
var mvMatrix;
var mvMatrixStack = [];
var pMatrix;
var pyramidVertexPositionBuffer;
var pyramidVertexColorBuffer;
var rPyramid;
var lastTime;
function initGL(canvas) {
  try {
    gl = canvas.getContext("experimental-webgl");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
  } catch (e) { }
  if (!gl) {
    alert("Could not initialise WebGL, sorry :-(");
  }
}
function getShader(gl, id) {
  var shaderScript = document.getElementById(id);
  if (!shaderScript) {
    return null;
  }
  var str = "";
  var k = shaderScript.firstChild;
  while (k) {
    if (k.nodeType == 3) {
      str += k.textContent;
    }
    k = k.nextSibling;
  }
  var shader;
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;
  }
  gl.shaderSource(shader, str);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert(gl.getShaderInfoLog(shader));
    return null;
  }
  return shader;
}
/**
 * Инициализация шейдеров для 3D объекта
 */
function initShaders() {
  var fragmentShader = getShader(gl, "shader-fs");
  var vertexShader = getShader(gl, "shader-vs");
  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert("Could not initialise shaders");
  }
  gl.useProgram(shaderProgram);
  shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
  shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
  gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);
  shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
}
function mvPushMatrix() {
  var copy = mat4.create();
  mat4.set(mvMatrix, copy);
  mvMatrixStack.push(copy);
}
function mvPopMatrix() {
  if (mvMatrixStack.length == 0) {
    throw "Invalid popMatrix!";
  }
  mvMatrix = mvMatrixStack.pop();
}
function setMatrixUniforms() {
  gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
  gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}
function degToRad(degrees) {
  return (degrees * Math.PI) / 180;
}
/**
 * Инициализация буфера для координат вершин треугольника и их цветов
 * 
 */
function initBuffers() {
  // Создание буфера
  pyramidVertexPositionBuffer = gl.createBuffer();

  // Все последующие операции будут происходить над буфером
  gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexPositionBuffer);

  // Матрица граней
  var vertices = [
    0.0, pyramidH, 0.0, -pyramidB, -1.0, pyramidA, pyramidB, -1.0, pyramidA, // Передняя грань
    0.0, pyramidH, 0.0, pyramidB, -1.0, pyramidA, pyramidB, -1.0, -pyramidA, // Правая грань
    0.0, pyramidH, 0.0, pyramidB, -1.0, -pyramidA, -pyramidB, -1.0, -pyramidA, // Задняя грань
    0.0, pyramidH, 0.0, -pyramidB, -1.0, -pyramidA, -pyramidB, -1.0, pyramidA,  // Левая грань
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
  pyramidVertexPositionBuffer.itemSize = 3;
  pyramidVertexPositionBuffer.numItems = 12;
  pyramidVertexColorBuffer = gl.createBuffer();


  // Все последующие операции будут происходить над буфером
  gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexColorBuffer);

  // Матрица цвета
  var colors = [
    1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, // Передняя грань
    1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, // Правая грань
    1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, // Задняя грань
    1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, // Левая грань
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
  pyramidVertexColorBuffer.itemSize = 4;
  pyramidVertexColorBuffer.numItems = 12;

  // Матрица цвета
  colors = [
    [0.0, 0.0, 0.0, 1.0], // Передняя грань
    [0.0, 0.0, 0.0, 0.8], // Задняя грань
    [0.0, 0.0, 0.0, 0.6], // Верхняя грань
    [0.0, 0.0, 0.0, 0.4], // Нижняя грань
    [0.0, 0.0, 0.0, 0.2], // Правая грань
    [0.0, 0.0, 0.0, 0.0], // Левая грань
  ];
  var unpackedColors = [];
  for (var i in colors) {
    var color = colors[i];
    for (var j = 0; j < 4; j++) {
      unpackedColors = unpackedColors.concat(color);
    }
  }
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(unpackedColors), gl.STATIC_DRAW);
}

/**
 * Отрисовка объекта на сцена
 */
function drawScene() {
  // Загрузка размеров
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);

  // Очищение canvas для подготовки к отрисовке
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // Установка перспективы, с которой необходимо обозревать сцену
  mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

  // Установка единичной матрицы
  mat4.identity(mvMatrix);
  mat4.translate(mvMatrix, [0, 0.0, -100.0]);
  mvPushMatrix();
  mat4.rotate(mvMatrix, degToRad(rPyramid), [0, 1, 0]);

  // Отрисовка матрицы
  gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexPositionBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, pyramidVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
  gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexColorBuffer);
  gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, pyramidVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);

  // Перенос изменений в переменной mvMatrix на видеокарту
  setMatrixUniforms();
  gl.drawArrays(gl.TRIANGLES, 0, pyramidVertexPositionBuffer.numItems);
  mvPopMatrix();
  mat4.translate(mvMatrix, [3.0, 0.0, 0.0]);
  mvPushMatrix();
}
function animate() {
  var timeNow = new Date().getTime();
  if (lastTime != 0) {
    var elapsed = timeNow - lastTime;

    rPyramid += (90 * elapsed) / 1000.0;
  }
  lastTime = timeNow;
}
/**
 * 
 */
function tick() {
  requestAnimFrame(tick);
  drawScene();
  animate();
}
function webGLStart() {
  mvMatrix = mat4.create();
  pMatrix = mat4.create();
  rPyramid = 0;
  lastTime = 0;
  let h = $("#inputPyramidH").val();
  let a = $("#inputPyramidA").val();
  let b = $("#inputPyramidA").val();
  pyramidH = parseFloat(h);
  pyramidA = parseFloat(a);
  pyramidB = parseFloat(b);
  var canvas = document.getElementById("CanvasPyramid");
  initGL(canvas);
  initShaders();
  initBuffers();
  gl.clearColor(0.0, 0.0, 0.0, 0.0);
  gl.enable(gl.DEPTH_TEST);
  tick();
}
