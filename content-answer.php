<?php
$users = dbGetUsers(2);
$tasks = dbGetTasks();
$answers = [];
if ($_SESSION['user']['role'] == 1) {
    $answers = getAnswers();
}
if ($_SESSION['user']['role'] == 2) {
    $answers = getAnswers($_SESSION['user']['id']);
}
?>
<div class="row">
    <div class="col-12">
        <div class="d-flex justify-content-end mt-3 mb-3">
            <button type="submit" name="submit" form="FromAnswerFilter" class="btn btn-primary m-1">Фильтр</button>
            <a href="/?r=answer" class="btn btn-outline-primary m-1">Сбросить фильтр</a>
        </div>
        <table class="table table-sm table-answer">
            <thead>
                <tr>
                    <th scope="col" class="th-num">#</th>
                    <th scope="col" class="th-username">Ученик</th>
                    <th scope="col" class="th-task-num">Номер задания</th>
                    <th scope="col" class="th-task-text">Задание</th>
                    <th scope="col" class="th-answer-result">Результат</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($_SESSION['user']['role'] == 1) : ?>
                    <form id="FromAnswerFilter" action="/?r=answer" method="post">
                        <tr>
                            <td></td>
                            <td>
                                <select name="user-id" class="form-control">
                                    <option value="-1">Все студенты</option>
                                    <?php
                                    foreach ($users as $key => $user) {
                                        if (isset($_POST['submit'])) {
                                            if ($user['id'] == $_POST['user-id']) {
                                                echo '<option value="' . $user['id'] . '" selected>' . $user['username'] . '</option>';
                                            } else {
                                                echo '<option value="' . $user['id'] . '">' . $user['username'] . '</option>';
                                            }
                                        } else {
                                            echo '<option value="' . $user['id'] . '">' . $user['username'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td>
                                <select name="task-id" class="form-control">
                                    <option value="-1">Все задания</option>

                                    <?php


                                    foreach ($tasks as $key => $task) {
                                        if (isset($_POST['submit'])) {
                                            if ($task['id'] == $_POST['task-id']) {
                                                echo '<option value="' . $task['id'] . '" selected>' . $task['id'] . '</option>';
                                            } else {
                                                echo '<option value="' . $task['id'] . '">' . $task['id'] . '</option>';
                                            }
                                        } else {
                                            echo '<option value="' . $task['id'] . '">' . $task['id'] . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td></td>
                            <td>
                                <select name="answer-result" class="form-control">
                                    <option value="-1">Все результаты</option>
                                    <?PHP
                                    if (isset($_POST['submit'])) {
                                        if ($_POST['answer-result'] == -1) {
                                            echo '<option value="0" >Ошибка</option>';
                                            echo '<option value="1">Верно</option>';
                                        }
                                        if ($_POST['answer-result'] == 0) {
                                            echo '<option value="0" selected>Ошибка</option>';
                                            echo '<option value="1">Верно</option>';
                                        }
                                        if ($_POST['answer-result'] == 1) {
                                            echo '<option value="0">Ошибка</option>';
                                            echo '<option value="1" selected>Верно</option>';
                                        }
                                    } else {
                                        echo '<option value="1">Верно</option>';
                                        echo '<option value="0">Ошибка</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                    </form>
                <?php endif ?>
                <?php foreach ($answers as $key => $answer) : ?>
                    <?php
                    if (isset($_POST['submit'])) {
                        $post = $_POST;
                        if (isset($post['user-id'])) {
                            if ($post['user-id'] != '-1' && $answer["user_id"] != $post['user-id']) {
                                continue;
                            }
                        }
                        if (isset($post['task-id'])) {
                            if ($post['task-id'] != '-1' && $answer["task_id"] != $post['task-id']) {
                                continue;
                            }
                        }
                        if (isset($post['answer-result'])) {
                            if ($post['answer-result'] != '-1') {
                                if ($post['answer-result'] != '0' && !checkResultTask($answer['id'])) {
                                    continue;
                                }
                                if ($post['answer-result'] != '1' && checkResultTask($answer['id'])) {
                                    continue;
                                }
                            }
                        }
                    }
                    ?>
                    <tr>
                        <th scope="row"><?= $key ?></th>
                        <td><?= dbGetNameUser($answer["user_id"]) ?></td>
                        <td><?= $answer["task_id"] ?></td>
                        <td><?= mb_strimwidth(dbGetTask($answer["task_id"])['text'], 0, 50, '...') ?></td>
                        <td>
                            <?php if (checkResultTask($answer['id'])) : ?>
                                <div class="badge badge-success">Верно</div>
                            <?php else : ?>
                                <div class="badge badge-danger">Ошибка</div>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>