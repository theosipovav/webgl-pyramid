<?php

/**
 * Стартовая страница
 * 
 * 
 */



?>



<section class="jumbotron text-center">
    <h1>Пирамида на основе WebGL</h1>
    <?php if ($isAuth) : ?>
        <div class="d-flex flex-column">
            <p>
                Здравствуйте, <?= $_SESSION['user']['username'] ?>
            </p>



        </div>
    <?php else : ?>
        <div class="d-flex flex-column justify-content-center">
            <p class="lead text-muted">Для дальнейшей работы необходимо авторизоваться на сайте или зарегистрироваться</p>
            <div class="d-flex justify-content-center">

                <a href="/?r=login" class="btn btn-primary mr-1 ml-1">Авторизация</a>
                <a href="/?r=reg" class="btn btn-outline-primary mr-1 ml-1">Регистрация</a>
            </div>
        </div>
    <?php endif ?>
</section>
<?php if ($isAuth) : ?>

    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <div class="d-flex flex-column" style="max-width: 500px;">
                <a href="/?r=tasks" class="btn btn-primary mt-1">Перейти к заданиям</a>
                <?php if (hasTeacher()) : ?>
                    <a href="/?r=answer" class="btn btn-outline-primary mt-1">Перейти к проверкам ответов</a>
                <?php endif ?>

            </div>
        </div>
    </div>
<?php endif ?>