<div class="row">
    <div class="col-12">
        <div class="singin">
            <form class="form-signin" action="/?r=login" method="POST" style="max-width: 450px;">
                <div class="d-flex flex-column align-items-center">
                    <img class="mb-4" src="/assets/img/login.png" alt="" width="150">
                    <h1 class="h3 mb-3 font-weight-normal text-center">Пожалуйста войдите в систему</h1>
                </div>
                <!-- Электронная почта -->
                <label for="inputEmail" class="sr-only">Электронная почта</label>
                <input type="email" id="inputEmail" class="form-control" name="mail" placeholder="Электронная почта" required autofocus>
                <!-- Пароль -->
                <label for="inputPassword" class="sr-only">Пароль</label>
                <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Пароль" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Войти</button>
                <p class=" text-center">
                    или
                </p>
                <a href="/?r=reg" class="btn btn-lg btn-outline-primary btn-block">Зарегистрироваться</a>
                <?php if ($messageError != '') : ?>
                    <div class="card text-white bg-danger mt-3 mb-3">
                        <div class="card-header">Внимание!</div>
                        <div class="card-body">
                            <h5 class="card-title">Не удалось выполнить авторизацию</h5>
                            <p class="card-text"><?= $messageError ?></p>
                        </div>
                    </div>
                <?php endif ?>
            </form>

        </div>
    </div>
</div>