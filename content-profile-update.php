<div class="row">
    <div class="col-12">
        <div class="singin">
            <form class="form-signin" action="/?r=profile-update" method="POST" style="max-width: 700px;">
                <div class="d-flex flex-column align-items-center">
                    <img class="mb-4" src="/assets/img/login.png" alt="" width="150">
                    <h1 class="h3 mb-3 font-weight-normal text-center">Редактирование профиля</h1>

                </div>
                <!-- Электронная почта -->
                <div class="form-group row">
                    <label for="inputMail" class="col-sm-4 col-form-label">Электронная почта</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="inputMail" name="mail" value="<?= $_SESSION['user']['mail'] ?>" required autofocus />
                    </div>
                </div>
                <!-- Пароль -->
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Пароль</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="inputPassword" name="password" value="<?= $_SESSION['user']['password'] ?>" required />
                    </div>
                </div>
                <!-- Повторите пароль -->
                <div class="form-group row">
                    <label for="inputRepeatPassword" class="col-sm-4 col-form-label">Повторите пароль</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="inputRepeatPassword" name="password-repeat" value="<?= $_SESSION['user']['password'] ?>" required />
                    </div>
                </div>
                <!-- Отображаемое имя -->
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">Отображаемое имя</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputUsername" name="username" value="<?= $_SESSION['user']['username'] ?>" required />
                    </div>
                </div>
                <!-- Роль -->
                <div class="form-group row">
                    <label for="inputRole" class="col-sm-4 col-form-label">Роль</label>
                    <div class="col-sm-8">
                        <select name="role" id="inputRole" class="form-control" disabled="true" required>
                            <option value="-1" disabled>Выберите роль</option>
                            <option value="1" <?php if ($_SESSION['user']['role'] == '1') echo 'selected'; ?>>Учитель</option>
                            <option value="2" <?php if ($_SESSION['user']['role'] == '2') echo 'selected'; ?>>Ученик</option>
                        </select>
                    </div>
                </div>
                <!-- Ваш преподаватель -->
                <?php if (!hasTeacher()) : ?>
                    <div id="ContainerSelectTeachers" class="form-group row">
                        <label for="selectTeachers" class="col-sm-4 col-form-label">Ваш преподаватель</label>
                        <div class="col-sm-8">
                            <select name="select-teachers" id="selectTeachers" class="form-control">
                                <option value="-1" disabled>Выберите преподавателя</option>
                                <?php foreach (getTeachers() as $key => $user) : ?>
                                    <option value="<?= $user["id"] ?>"><?= $user["username"] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                <?php endif ?>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Обновить</button>
                <?php if ($messageError != '') : ?>
                    <div class="card text-white bg-danger mt-3 mb-3">
                        <div class="card-header">Внимание!</div>
                        <div class="card-body">
                            <h5 class="card-title">Не удалось обновить профиль</h5>
                            <p class="card-text"><?= $messageError ?></p>
                        </div>
                    </div>
                <?php endif ?>
                <?php if ($messageSuccess != '') : ?>
                    <div class="card text-white bg-success mt-3 mb-3">
                        <div class="card-header">Внимание!</div>
                        <div class="card-body">
                            <p class="card-text"><?= $messageSuccess ?></p>
                        </div>
                    </div>
                <?php endif ?>
            </form>

        </div>
    </div>
</div>