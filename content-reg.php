<div class="row">
    <div class="col-12">
        <div class="singin">
            <form class="form-signin" action="/?r=reg" method="POST" style="max-width: 700px;">
                <div class="d-flex flex-column align-items-center">
                    <img class="mb-4" src="/assets/img/login.png" alt="" width="150">
                    <h1 class="h3 mb-3 font-weight-normal text-center">Регистрация нового пользователя</h1>

                </div>
                <!-- Электронная почта -->
                <div class="form-group row">
                    <label for="inputMail" class="col-sm-4 col-form-label">Электронная почта</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="inputMail" name="mail" aria-describedby="emailHelp" required autofocus />
                    </div>
                </div>
                <!-- Пароль -->
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Пароль</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="inputPassword" name="password" required />
                    </div>
                </div>
                <!-- Повторите пароль -->
                <div class="form-group row">
                    <label for="inputRepeatPassword" class="col-sm-4 col-form-label">Повторите пароль</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="inputRepeatPassword" name="password-repeat" required />
                    </div>
                </div>
                <!-- Отображаемое имя -->
                <div class="form-group row">
                    <label for="inputUsername" class="col-sm-4 col-form-label">Отображаемое имя</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="inputUsername" name="username" required aria-describedby="validationServer03Feedback" />
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            Пожалуйста, вводите только русский буквы
                        </div>
                    </div>
                </div>
                <!-- Роль -->
                <div class="form-group row">
                    <label for="inputRole" class="col-sm-4 col-form-label">Роль</label>
                    <div class="col-sm-8">
                        <select name="role" id="inputRole" class="form-control">
                            <option value="-1" disabled>Выберите роль</option>
                            <option value="1">Учитель</option>
                            <option value="2" selected>Ученик</option>
                        </select>
                    </div>
                </div>
                <!-- Ваш преподаватель -->
                <div id="ContainerSelectTeachers" class="form-group row hidden">
                    <label for="selectTeachers" class="col-sm-4 col-form-label">Ваш преподаватель</label>
                    <div class="col-sm-8">
                        <select name="select-teachers" id="selectTeachers" class="form-control">
                            <option value="-1" disabled>Выберите преподавателя</option>
                            <?php foreach (getTeachers() as $key => $user) : ?>
                                <option value="<?= $user["id"] ?>"><?= $user["username"] ?></option>
                            <?php endforeach ?>

                        </select>
                    </div>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Зарегистрироваться</button>
                <p class="text-center">
                    или
                </p>
                <a href="/?r=login" class="btn btn-lg btn-outline-primary btn-block">Выполните вход</a>
                <?php if ($messageError != '') : ?>
                    <div class="card text-white bg-danger mt-3 mb-3">
                        <div class="card-header">Внимание!</div>
                        <div class="card-body">
                            <h5 class="card-title">Не удалось выполнить регистрацию</h5>
                            <p class="card-text"><?= $messageError ?></p>
                        </div>
                    </div>
                <?php endif ?>
            </form>

        </div>
    </div>
</div>