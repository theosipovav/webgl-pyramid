<?php

global $messageError;
global $messageSuccess;
if (isset($_POST['submit'])) {
    if (dbAddTask($_POST['text'], $_POST['decision'], $_POST['answer-v'], $_POST['answer-h'], $_POST['answer-a'], $_POST['user_id'])) {
        $messageSuccess = "Задание добавлено";
    } else {
        $messageError = dbLastError();
    }
}

?>


<div class="row">
    <div class="col-12">
        <div class="singin">
            <?php if ($_SESSION['user']['role'] == 1) : ?>
                <form class="form-signin" action="/?r=task-add" method="POST" style="max-width: 700px;">
                    <div class="d-flex flex-column align-items-center">
                        <img class="mb-4" src="/assets/img/tasks.png" alt="" width="150">
                        <h1 class="h3 mb-3 font-weight-normal text-center">Добавить новое задание</h1>

                    </div>
                    <!-- Текст задания -->
                    <div class="form-group row">
                        <label for="textareaText" class="col-sm-4 col-form-label">Текст задания</label>
                        <div class="col-sm-8">
                            <textarea name="text" id="textareaText" class="form-control" rows="10" required autofocus></textarea>
                        </div>
                    </div>
                    <!-- Искомая переменная -->
                    <div class="form-group row">
                        <label for="selectDecision" class="col-sm-4 col-form-label">Искомая переменная</label>
                        <div class="col-sm-8">
                            <select name="decision" id="selectDecision" class="form-control" required>
                                <option value="h">Высота пирамиды</option>
                                <option value="a">Длина стороны основания</option>
                                <option value="v">Объем пирамиды</option>
                            </select>
                        </div>
                    </div>
                    <!-- Высота пирамиды -->
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-4 col-form-label">Высота пирамиды</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" class="form-control" id="inputPyramidH" name="answer-h" required />
                        </div>
                    </div>
                    <!-- Длина стороны основания -->
                    <div class="form-group row">
                        <label for="inputPyramidA" class="col-sm-4 col-form-label">Длина стороны основания</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" class="form-control" id="inputPyramidA" name="answer-a" required />
                        </div>
                    </div>
                    <!-- Объем пирамиды -->
                    <div class="form-group row">
                        <label for="inputPyramidV" class="col-sm-4 col-form-label">Объем пирамиды</label>
                        <div class="col-sm-8">
                            <input type="number" min="1" class="form-control" id="inputPyramidV" name="answer-v" required />
                        </div>
                    </div>
                    <input type="hidden" name="user_id" value="<?= $_SESSION['user']['id'] ?>">

                    <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Добавить задание</button>

                    <?php if ($messageError != '') : ?>
                        <div class="card text-white bg-danger mt-3 mb-3">
                            <div class="card-header">Внимание!</div>
                            <div class="card-body">
                                <h5 class="card-title">Не удалось обновить профиль</h5>
                                <p class="card-text"><?= $messageError ?></p>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if ($messageSuccess != '') : ?>
                        <div class="card text-white bg-success mt-3 mb-3">
                            <div class="card-header">Внимание!</div>
                            <div class="card-body">
                                <p class="card-text"><?= $messageSuccess ?></p>
                            </div>
                        </div>
                    <?php endif ?>
                </form>
            <?php else : ?>
                <div class="card text-white bg-warning mb-3" style="margin: auto;">
                    <div class="card-header">Внимание</div>
                    <div class="card-body">
                        <h5 class="card-title">Добавить новое задание может только преподаватель</h5>
                    </div>
                <?php endif ?>

                </div>
        </div>
    </div>