<?php
$task = dbGetTask($_GET['id']);
$nextTask = getNextTask($task['id']);
$currentAnswer = getAnswerForTask($_SESSION['user']['id'], $task['id']);
?>

<div class="row">
    <div class="col">
        <h2>Задание №<?= $task['id'] ?></h2>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <h3>Текст задания:</h3>
    </div>
    <div class="col-12">
        <p>
            <?= $task['text'] ?>
        </p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12 col-md-7 d-flex justify-content-center align-items-center">
        <canvas id="CanvasPyramid" style="border: none;" width="500" height="500"></canvas>
        <script id="shader-fs" type="x-shader/x-fragment">
            precision mediump float;
            varying vec4 vColor;
            void main(void) 
            {
                gl_FragColor = vColor;
            }
        </script>
        <script id="shader-vs" type="x-shader/x-vertex">
            attribute vec3 aVertexPosition;
        attribute vec4 aVertexColor;
        uniform mat4 uMVMatrix;
        uniform mat4 uPMatrix;
        varying vec4 vColor;
        void main(void) 
        {
        gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
        vColor = aVertexColor;
        }
        </script>
        <script src="assets/js/glMatrix-0.9.5.min.js"></script>
        <script src="assets/js/webgl-utils.js"></script>
    </div>
    <div class="col-12 col-md-5">
        <div class="row">
            <div class="col-12">
                <form id="FormTask">
                    <div class="form-group row">
                        <label for="inputPyramidH" class="col-sm-8 col-form-label">Высота пирамиды</label>
                        <div class="col-sm-4">
                            <?php if ($task['decision'] == 'h') : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidH" value="1">
                            <?php else : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidH" value="<?= $task["answer_h"] ?>">
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidA" class="col-sm-8 col-form-label">Длина стороны основания</label>
                        <div class="col-sm-4">
                            <?php if ($task['decision'] == 'a') : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidA" value="1">
                            <?php else : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidA" value="<?= $task["answer_a"] ?>">
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPyramidV" class="col-sm-8 col-form-label">Объем пирамиды</label>
                        <div class="col-sm-4">
                            <?php if ($task['decision'] == 'v') : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidV" value="1">
                            <?php else : ?>
                                <input type="number" min="1" class="form-control" id="inputPyramidV" value="<?= $task["answer_v"] ?>">
                            <?php endif ?>
                        </div>
                    </div>
                    <input type="hidden" id="inputTask" value="<?= $task['id'] ?>">
                </form>
                <hr>
            </div>
            <div class="col-12">
                <button class="btn btn-primary" onclick="setAnswer()">Отправить ответ</button>
                <hr>
            </div>
            <?php if ($currentAnswer != false) : ?>
                <div class="col-12">
                    <h5 class="h5 text-info">Ваш ответ</h5>
                    <table class="table table-borderless table-sm table-info">
                        <tbody>
                            <tr>
                                <th>Высота пирамиды</th>
                                <td><?= $currentAnswer['h'] ?></td>
                            </tr>
                            <tr>
                                <th>Длина стороны основания</th>
                                <td><?= $currentAnswer['a'] ?></td>
                            </tr>
                            <tr>
                                <th>Объем пирамиды</th>
                                <td><?= $currentAnswer['v'] ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php

                    ?>
                    <hr>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12 col-md-2">
        <button class="btn btn-warning mb-1" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Показать решение и ответ
        </button>
    </div>
    <div class="col-12 col-md-8">
        <div class="collapse mb-1" id="collapseExample">
            <div class="card card-body d-flex flex-column align-content-center">
                <p>
                    Правильная четырехугольная пирамида - пирамида, у которой основанием является квадрат и грани равные равнобедренные треугольники.
                </p>
                <img src="assets/img/answer_1.jpg" alt="" srcset="" style="height: 200px; object-fit: contain;">
                <h3 class="h3 text-center mt-1">V=1/3ha<sup>2</sup></h3>
                <table class="table table-bordered mt-1">
                    <thead>
                        <tr>
                            <th>Высота пирамиды</th>
                            <th>Длина стороны основания</th>
                            <th>Объем пирамиды</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $task["answer_h"] ?></td>
                            <td><?= $task["answer_a"] ?></td>
                            <td><?= $task["answer_v"] ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-2">
        <?php if ($nextTask != false) : ?>
            <a href="/?r=task&id=<?= $nextTask["id"] ?>" class="btn btn-info mb-1" type="button">
                следующее задание
            </a>
        <?php endif ?>
    </div>
</div>