<div class="row">
    <div class="col-12">
        <h2 class="h2 mb-3">Задания</h2>
    </div>
    <div class="col-12">
        <?php foreach (dbGetTasksForStudent($_SESSION['user']['id']) as $key => $task) : ?>
            <div class="card bg-light mb-3">
                <div class="card-header">Задание №<?= $task["id"] ?></div>
                <div class="card-body">
                    <p class="card-text"><?= $task["text"] ?></p>
                </div>
                <div class="card-footer ">

                    <div class="row">
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-center text-info">
                                <span class="mr-1">Автор:</span><span><?= dbGetUserForId($task['user_id'])['username']; ?></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-end flex-grow-1">
                                <a href="/?r=task&id=<?= $task["id"] ?>" class="btn btn-primary m-1">Перейти</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>