<div class="row">
    <div class="col-12">
        <h2 class="h2 mb-3">Задания</h2>
    </div>

    <div class="col-12 mb-3">
        <a href="/?r=tasks&filter=1" class="btn btn-secondary">Ваши задания</a>
        <a href="/?r=tasks" class="btn btn-secondary">Все задания</a>
    </div>

    <div class="col-12">
        <?php foreach (dbGetTasks() as $key => $task) : ?>
            <?php
            if (isset($_GET['filter'])) {
                if ($task['user_id'] != $_SESSION['user']['id']) {
                    continue;
                }
            }
            ?>

            <div class="card bg-light mb-3">
                <div class="card-header">Задание №<?= $task["id"] ?></div>
                <div class="card-body">
                    <p class="card-text"><?= $task["text"] ?></p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-center text-info">
                                <span class="mr-1">Автор:</span><span><?= dbGetUserForId($task['user_id'])['username']; ?></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="d-flex justify-content-end flex-grow-1">
                                <button type="button" class="btn btn-danger btn-remove-task m-1" data-toggle="modal" data-target="#modalRemoveTask" data-task-id="<?= $task["id"] ?>">
                                    Удалить
                                </button>
                                <a href="/?r=task&id=<?= $task["id"] ?>" class="btn btn-primary m-1">Перейти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach ?>


    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalRemoveTask" tabindex="-1" aria-labelledby="modalRemoveTaskLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalRemoveTaskLabel">Удаление задания N <span>???</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Вы действительно хотите удалить задание?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-danger btn-ok">Удалить</button>
            </div>
        </div>
    </div>
</div>