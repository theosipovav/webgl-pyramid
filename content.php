<?php

global $isAuth;


// Маршрутизация
$fileRoute = 'content-home.php';
if (isset($_GET["r"])) {
    $route = $_GET["r"];

    if ($isAuth) {
        $fileRoute = 'content-' . $route . '.php';
        if ($route == 'reg') {
            $fileRoute = 'content-home.php';
        }
        if ($route == 'login') {
            $fileRoute = 'content-home.php';
        }
    } else {
        $fileRoute = 'content-home.php';
        ($route);
        if ($route == 'login') {
            $fileRoute = 'content-' . $route . '.php';
        }
        if ($route == 'reg') {
            $fileRoute = 'content-' . $route . '.php';
        }
    }
}
if (!file_exists($fileRoute)) {
    $fileRoute = 'content-404.php';
}

?>

<div class="container mt-3">
    <?php include_once $fileRoute; ?>
</div>