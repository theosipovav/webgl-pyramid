-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 23 2020 г., 16:19
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `id15082468_webglpyramid`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `user_id` int(11) NOT NULL COMMENT 'Автор ответа',
  `task_id` int(11) DEFAULT NULL COMMENT 'Задание',
  `h` double NOT NULL DEFAULT 0 COMMENT 'Высота пирамиды',
  `a` double NOT NULL DEFAULT 0 COMMENT 'Длина стороны основания',
  `v` double NOT NULL DEFAULT 0 COMMENT 'Длина стороны ребра',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи',
  `b` double NOT NULL DEFAULT 0 COMMENT 'Длина стороны ребра'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Ответы';

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `user_id`, `task_id`, `h`, `a`, `v`, `created_dt`, `update_dt`, `b`) VALUES
(71, 36, 10, 30, 3, 2250, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(72, 36, 3, 16, 36, 6912, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(73, 36, 5, 12, 24, 2304, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(74, 36, 9, 15, 30, 78, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(75, 36, 8, 30, 30, 90, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(76, 36, 1, 6, 10, 3200, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(77, 42, 5, 1666, 24, 2304, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0),
(78, 42, 4, 441, 36, 5184, '2020-10-23 16:16:45', '2020-10-23 16:16:45', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `parent_id` int(11) NOT NULL COMMENT 'Преподаватель',
  `child_id` int(11) NOT NULL COMMENT 'Ученик',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Группы учеников по преподавателям';

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `parent_id`, `child_id`, `created_dt`, `update_dt`) VALUES
(1, 33, 43, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(2, 33, 42, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(3, 33, 41, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(4, 33, 40, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(5, 33, 39, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(6, 33, 38, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(8, 34, 37, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(9, 34, 38, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(10, 34, 39, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(11, 34, 38, '2020-10-23 16:13:23', '2020-10-23 16:13:23'),
(12, 33, 36, '2020-10-23 16:13:23', '2020-10-23 16:13:23');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Наименование',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Роли';

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_dt`, `update_dt`) VALUES
(1, 'Учитель', '2020-10-23 16:13:17', '2020-10-23 16:13:17'),
(2, 'Ученик', '2020-10-23 16:13:17', '2020-10-23 16:13:17');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `text` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Текст задания',
  `decision` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Искомый параметр',
  `answer_v` double NOT NULL DEFAULT 0 COMMENT 'Объем пирамиды',
  `answer_h` double NOT NULL DEFAULT 0 COMMENT 'Высота пирамиды',
  `answer_a` double NOT NULL DEFAULT 0 COMMENT 'Длина стороны основания',
  `answer_b` double NOT NULL DEFAULT 0 COMMENT 'Длина стороны ребра',
  `user_id` int(11) NOT NULL COMMENT 'Автор задания',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Задания';

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `text`, `decision`, `answer_v`, `answer_h`, `answer_a`, `answer_b`, `user_id`, `created_dt`, `update_dt`) VALUES
(1, 'В правильной четырехугольной пирамиде высота равна 6 см, а cторона основания 10 см. Найдите её объем.', 'v', 200, 6, 10, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(2, 'В правильной четырехугольной пирамиде объем равен2886 см, а cторона основания 12 см. Найдите её высоту.', 'h', 288, 6, 12, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(3, 'В правильной четырехугольной пирамиде объем равен 6912 см, а её воста 16 см. Найдите cторону основания.', 'a', 6912, 16, 36, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(4, 'В правильной четырехугольной пирамиде объем равен 5184 см, а cторона основания 36 см. Найдите её высоту.', 'h', 5184, 12, 36, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(5, 'В правильной четырехугольной пирамиде объем равен 2304 см, а cторона основания 24 см. Найдите её высоту.', 'h', 2304, 12, 24, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(6, 'В правильной четырехугольной пирамиде объем равен 1152 см, а cторона основания 12 см. Найдите её высоту.', 'h', 1152, 24, 12, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(7, 'В правильной четырехугольной пирамиде высота равна 24 см, а cторона основания 24 см. Найдите её объем.', 'v', 4608, 24, 24, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(8, 'В правильной четырехугольной пирамиде высота равна 30 см, а cторона основания 30 см. Найдите её объем.', 'v', 9000, 30, 30, 0, 33, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(9, 'В правильной четырехугольной пирамиде высота равна 15 см, а cторона основания 30 см. Найдите её объем.', 'v', 4500, 15, 30, 0, 34, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(10, 'В правильной четырехугольной пирамиде объем равен 2250 см, а её воста 30 см. Найдите сторону основания.', 'a', 2250, 30, 15, 0, 34, '2020-10-23 16:13:09', '2020-10-23 16:13:10'),
(12, 'Тестовое задание учителя 3', 'v', 10, 10, 300, 0, 35, '2020-10-23 16:13:09', '2020-10-23 16:13:10');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `mail` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Электронный адрес',
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Пароль для входа в систему',
  `username` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Имя пользователя для входа в систему',
  `role` int(11) NOT NULL COMMENT 'Роль пользователя',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Пользователи';

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `mail`, `password`, `username`, `role`, `created_dt`, `update_dt`) VALUES
(33, 'teacher-1@mail.ru', '12345678', 'Иванов И.И.', 1, '2020-10-09 19:41:02', '2020-10-23 16:12:50'),
(34, 'teacher-2@mail.ru', '12345678', 'Григорьев А.И.', 1, '2020-10-09 19:41:02', '2020-10-23 16:12:50'),
(35, 'teacher-3@mail.ru', '12345678', 'Петров А.А.', 1, '2020-10-09 19:41:02', '2020-10-23 16:12:50'),
(36, 'student-1@mail.ru', '12345678', 'Алексеев А.А.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(37, 'student-2@mail.ru', '12345678', 'Иванова А.П.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(38, 'student-3@mail.ru', '12345678', 'Дмитров Д.Д.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(39, 'student-4@mail.ru', '12345678', 'Васинина А.А.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(40, 'student-5@mail.ru', '12345678', 'Рязанова А.А.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(41, 'student-6@mail.ru', '12345678', 'Никанорова И.В.', 2, '2020-10-09 19:42:54', '2020-10-23 16:12:50'),
(42, 'kishilevg@mail.ru', 'Nastya333', 'teacher-1@mail.ru', 2, '2020-10-06 21:11:35', '2020-10-23 16:12:50'),
(43, 'teacher-101@mail.ru', '12345678', 'teacher-1@mail.ru', 2, '2020-10-06 21:11:35', '2020-10-23 16:12:50');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_FK` (`user_id`);

--
-- Индексы таблицы `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `curators_FK_user__parent` (`parent_id`),
  ADD KEY `curators_FK_user__child` (`child_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_FK_user` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_FK` (`role`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT для таблицы `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=44;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `group`
--
ALTER TABLE `group`
  ADD CONSTRAINT `curators_FK_user__child` FOREIGN KEY (`child_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `curators_FK_user__parent` FOREIGN KEY (`parent_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_FK_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_FK` FOREIGN KEY (`role`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
