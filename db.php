<?php
define("db_host", "localhost");
// define("db_user", "id15082468_admin_db");
// define("db_password", "q\khS{bb1MXFHT\k");
define("db_user", "mysql");
define("db_password", "mysql");
define("db_name", "id15082468_webglpyramid");
function writeLog($message)
{
    try {
        $filename = 'log_' . date('YmdHis') . '.txt';
        $fp = fopen("logs//" . $filename, 'w');
        fwrite($fp, $message);
        fclose($fp);
    } catch (Exception $e) {
        echo "Критическая ошибка!<br>";
        echo $e;
        die();
        exit();
    }
}
$mysqli = new mysqli(db_host, db_user, db_password, db_name);
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: (" . $nMysqli->connect_errno . ") " . $nMysqli->connect_error;
    $error = "Не удалось подключиться к MySQL: (" . $nMysqli->connect_errno . ") " . $nMysqli->connect_error;
    writeLog($error);
    exit();
}
/**
 * Получить последнюю ошибку с БД
 */
function dbLastError()
{
    global $mysqli;
    return $mysqli->error;
}
/**
 * Создание нового пользователя 
 */
function dbCreateUser($mail, $password, $username, $role)
{
    global $mysqli;
    $user = dbFindUser($mail);
    if (is_array($user)) {
        return false;
    }
    $query = "INSERT INTO `users` (`mail`, `password`, `username`, `role`, `created_dt`) VALUES ('$mail', '$password', '$username', '$role', '2020-10-06 21:11:35')";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}
/**
 * Обновление записи о пользователе
 */
function dbUpdateUser($id, $mail, $password, $username)
{
    global $mysqli;
    $query = "UPDATE `users` SET `mail` = '$mail', `password` = '$password', `username` = '$username' WHERE `users`.`id` = " . $id;
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        $fp = fopen('log.txt', 'w');
        fwrite($fp, 'Не удалось обновить существующую запись пользователя.');
        fclose($fp);
        return false;
    }
}
/**
 * База данных. Получить всех пользователей
 */
function dbGetUsers($roleId = null)
{
    global $mysqli;
    $users = [];

    if ($roleId == null) {
        $query = "SELECT * FROM `users`";
    } else {
        $query = "SELECT * FROM `users` WHERE `role` = '$roleId'";
    }
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $user = [];
            $user["id"] = $row[0];
            $user["mail"] = $row[1];
            $user["password"] = $row[2];
            $user["username"] = $row[3];
            $user["role"] = $row[4];
            $user["created_dt"] = $row[5];
            $users[] = $user;
        }
        $res->close();
    }
    return $users;
}
/**
 * База данных. Получить пользователя по его идентификатору
 */
function dbGetUserForId($id)
{
    global $mysqli;
    $user = false;
    $query = "SELECT * FROM `users` WHERE `id` = '$id'";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_object()) {
            $user = [];
            $user["id"] = $row->id;
            $user["mail"] = $row->mail;
            $user["password"] = $row->password;
            $user["username"] = $row->username;
            $user["role"] = $row->role;
            $user["created_dt"] = $row->created_dt;
            break;
        }
        $res->close();
    }
    return $user;
}

/**
 * Поиск пользователя
 */
function dbFindUser($mail, $password = '')
{
    global $mysqli;
    $user = false;
    if ($password == '') {
        $query = "SELECT * FROM `users` WHERE `mail` = '$mail'";
    } else {
        $query = "SELECT * FROM `users` WHERE `mail` = '$mail' and `password` = '$password'";
    }
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $user = [];
            $user["id"] = $row[0];
            $user["mail"] = $row[1];
            $user["password"] = $row[2];
            $user["username"] = $row[3];
            $user["role"] = $row[4];
            $user["created_dt"] = $row[5];
            break;
        }
        $res->close();
    }
    return $user;
}
/**
 * Получить все задания 
 */
function dbGetTasks()
{
    global $mysqli;
    $tasks = [];
    $query = "SELECT * FROM `tasks`";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_object()) {
            $task = [];
            $task["id"] = $row->id;
            $task["text"] = $row->text;
            $task["decision"] = $row->decision;
            $task["answer"] = $row->answer;
            $task["user_id"] = $row->user_id;
            $tasks[] = $task;
        }
        $res->close();
    }
    return $tasks;
}
/**
 * Получить все задания для студента 
 */
function dbGetTasksForStudent($user_id)
{
    global $mysqli;
    $tasks = [];
    $teachers = getIndividualTeachers($user_id);
    foreach ($teachers as $key => $user) {
        $id  = $user['id'];
        $query = "select * from tasks where user_id = '$id'";
        if ($res = $mysqli->query($query)) {
            while ($row = $res->fetch_object()) {
                $task = [];
                $task["id"] = $row->id;
                $task["text"] = $row->text;
                $task["decision"] = $row->decision;
                $task["answer"] = $row->answer;
                $task["user_id"] = $row->user_id;
                $tasks[] = $task;
            }
            $res->close();
        }
    }
    return $tasks;
}
/**
 * Получить задание по его идентификатору
 * Результат: ARRAY (id, text, decision, answer_v, answer_h, answer_a, answer_b)
 */
function dbGetTask($id)
{
    global $mysqli;
    $task = false;
    $query = "SELECT * FROM `tasks` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $task = [];
            $task["id"] = $row[0];
            $task["text"] = $row[1];
            $task["decision"] = $row[2];
            $task["answer_v"] = $row[3];
            $task["answer_h"] = $row[4];
            $task["answer_a"] = $row[5];
            $task["answer_b"] = $row[6];
            break;
        }
        $res->close();
    }
    return $task;
}
/**
 * Получить список не выполненных задач для ученика
 */
function dbGetNewTasks($userId)
{
    global $mysqli;
    $query = "SELECT id, `text`, decision, answer_v, answer_h, answer_a, answer_b FROM id15082468_webglpyramid.tasks where id not in (SELECT task_id FROM id15082468_webglpyramid.answers where user_id = $userId)";
    $tasks = [];
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $task = [];
            $task["id"] = $row[0];
            $task["text"] = $row[1];
            $task["decision"] = $row[2];
            $task["answer"] = $row[3];
            $tasks[] = $task;
        }
        $res->close();
    }
    return $tasks;
}
/**
 * Получить имя роли
 */
function dbGetNameRole($id)
{

    global $mysqli;
    $nameRole = false;
    $query = "SELECT * FROM `roles` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $nameRole = $row[1];
            break;
        }
        $res->close();
    }
    return $nameRole;
}
/**
 * Получить отображаемое имя пользователя
 */
function dbGetNameUser($id)
{

    global $mysqli;
    $name = false;
    $query = "SELECT * FROM `users` WHERE `id` = $id";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $name = $row[3];
            break;
        }
        $res->close();
    }
    return $name;
}
function dbSetAnswer($task, $h, $a, $v)
{
    global $mysqli;
    session_start();
    if (isset($_SESSION['user']['id'])) {
        $user = $_SESSION['user']['id'];

        $answer = false;
        $query = "SELECT id FROM `answers` where user_id = $user and task_id = $task";
        if ($res = $mysqli->query($query)) {
            while ($row = $res->fetch_row()) {
                $answer = [];
                $answer["id"] = $row[0];
                $answer["user_id"] = $row[1];
                $answer["task_id"] = $row[2];
                $answer["h"] = $row[3];
                $answer["a"] = $row[4];
                $answer["v"] = $row[5];
                break;
            }
            $res->close();
        }
        if ($answer != false) {
            // Обновление существующего ответа
            $query = "UPDATE `answers` SET `h` = '$h', `a` = '$a', `v` = '$v' WHERE `answers`.`id` = " . $answer["id"];
            if ($mysqli->query($query) === TRUE) {
                return true;
            } else {
                $fp = fopen('log.txt', 'w');
                fwrite($fp, 'Не удалось обновить существующую запись.');
                fclose($fp);
                return false;
            }
        } else {
            // Создание нового ответа
            $query = "INSERT INTO `answers` (`id`, `user_id`, `task_id`, `h`, `a`, `v`) VALUES (NULL, '$user', '$task', '$h', '$a', '$v')";
            if ($mysqli->query($query) === TRUE) {
                return true;
            } else {
                $fp = fopen('log.txt', 'w');
                fwrite($fp, 'Не удалось создать запись нового ответа');
                fclose($fp);
                return false;
            }
        }
    } else {
        $fp = fopen('log.txt', 'w');
        fwrite($fp, 'Пользователь не авторизован');
        return false;
    }
}

/**
 * Получить все ответы студентов
 */
function getAnswers($userId = null)
{
    global $mysqli;
    $answers = [];

    if ($userId == null) {
        $query = "SELECT * FROM `answers`";
    } else {
        $query = "SELECT * FROM `answers` WHERE `answers`.`user_id` = '$userId'";
    }

    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $answer = [];
            $answer["id"] = $row[0];
            $answer["user_id"] = $row[1];
            $answer["task_id"] = $row[2];
            $answer["h"] = $row[3];
            $answer["a"] = $row[4];
            $answer["v"] = $row[5];
            $answers[] = $answer;
        }
        $res->close();
    }
    return $answers;
}

/**
 * Получить ответ по его идентификатору
 * Результат: ARRAY(id, user_id, task_id, h, a, v)
 */
function getAnswer($id)
{
    global $mysqli;
    $answer = false;
    $query = "SELECT * FROM `answers` WHERE `id` = '$id'";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $answer = [];
            $answer["id"] = $row[0];
            $answer["user_id"] = $row[1];
            $answer["task_id"] = $row[2];
            $answer["h"] = $row[3];
            $answer["a"] = $row[4];
            $answer["v"] = $row[5];
            break;
        }
        $res->close();
    }
    return $answer;
}
/**
 * Получить ответ по идентификатору пользователя и задачи
 * Результат: ARRAY(id, user_id, task_id, h, a, v)
 */
function getAnswerForTask($userId, $taskId)
{
    global $mysqli;
    $answer = false;
    $query = "SELECT * FROM `answers` WHERE user_id = $userId and task_id = $taskId";
    if ($res = $mysqli->query($query)) {
        while ($row = $res->fetch_row()) {
            $answer = [];
            $answer["id"] = $row[0];
            $answer["user_id"] = $row[1];
            $answer["task_id"] = $row[2];
            $answer["h"] = $row[3];
            $answer["a"] = $row[4];
            $answer["v"] = $row[5];
            break;
        }
        $res->close();
    }
    return $answer;
}

/**
 * База данных. Создание нового задания.
 */
function dbAddTask($text, $decision, $answerV, $answerH, $answerA, $user_id)
{
    global $mysqli;
    $query = "INSERT INTO `tasks` (`id`, `text`, `decision`, `answer_v`, `answer_h`, `answer_a`, `answer_b`, `user_id`) VALUES (NULL, '$text', '$decision', '$answerV', '$answerH', '$answerA', '0', '$user_id')";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}

/**
 * База данных. Удаление задания по его идентификатору.
 */
function dbRemoveTask($id)
{
    global $mysqli;
    $query = "DELETE FROM `tasks` WHERE id = $id;";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        $filename = 'log_' . date('YmdHis') . '.txt';
        $fp = fopen($filename, 'w');
        fwrite($fp, dbLastError());
        fclose($fp);
        return false;
    }
}




/**
 * База данных получить группы учеников
 */
function dbGetGroupsForChildId($child_id)
{
    global $mysqli;
    $groups = [];
    $query = "select id, parent_id, child_id from `group` where `group`.child_id = '$child_id';";
    if ($res = $mysqli->query($query)) {
        while ($obj = $res->fetch_object()) {
            $group = [];
            $group['id'] = $obj->id;
            $group['parent_id'] = $obj->parent_id;
            $group['child_id'] = $obj->child_id;
            $groups[] = $group;
            break;
        }
        $res->close();
    }
    return $groups;
}

/**
 * База данных. Прикрепить ученика к преподавателю.
 */
function dbCreateGroup($parent_id, $child_id)
{
    global $mysqli;
    $query = "INSERT INTO `group` (`id`, `parent_id`, `child_id`) VALUES (NULL, '$parent_id', '$child_id')";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}
/**
 * База данных. Удалить ученика у преподавателей.
 */
function dbRemoveGroup($child_id)
{
    global $mysqli;
    $query = "DELETE FROM `group` WHERE `group`.`child_id` = '$child_id'";
    if ($mysqli->query($query) === TRUE) {
        return true;
    } else {
        return false;
    }
}
