<?php

// Сессия
session_start();
// Глобальные переменные
$isAuth = false;
$messageError = "";
$messageSuccess = "";

if (isset($_SESSION['user'])) {
    $isAuth = true;
}



if ($_GET["r"] == 'login' && isset($_POST['submit'])) {
    login();
}
if ($_GET["r"] == 'logout' && isset($_POST['submit'])) {
    var_dump($_POST);
}
if ($_GET["r"] == 'reg' && isset($_POST['submit'])) {
    reg();
}
if ($_GET["r"] == 'profile-update' && isset($_POST['submit'])) {
    updateProfile();
}

/**
 * Регистрация пользователя в системе
 */
function reg()
{
    $post = $_POST;
    if (dbCreateUser($post["mail"], $post["password"], $post["username"], $post["role"])) {
        if ($post['role'] == 2) {
            if (isset($post["select-teachers"])) {
                $user = dbFindUser($post["mail"], $post["password"]);
                if (dbCreateGroup($post["select-teachers"], $user["id"])) {
                    global $messageError;
                    $messageError = "Не удалось прикрепить ученика к преподавателю";
                }
            }
        }
        login();
    } else {
        global $messageError;
        $messageError = "Пользователь с таким почтовым адресом уже зарегистрирован";
    }
}

/**
 * Авторизация пользователя в системе
 */
function login()
{
    global $isAuth;
    global $messageError;
    $post = $_POST;
    $user = dbFindUser($post["mail"], $post["password"]);
    if (is_array($user)) {
        $_SESSION['user'] =  $user;
        $isAuth = true;
    } else {
        $messageError = "Пользователь не найден. Возможно введен неверный логин или пароль.";
    }
}




/**
 * Обновление записи о пользователе
 */
function updateProfile()
{
    global $messageError;
    global $messageSuccess;
    global $isAuth;
    if (isset($_SESSION['user']['id'])) {
        $id = $_SESSION['user']['id'];
        $role = $_SESSION['user']['role'];
        if (isset($_POST)) {
            $post = $_POST;


            $mail = $post['mail'];
            $password = $post['password'];
            $passwordRepeat = $post['password-repeat'];
            $username = $post['username'];
            if ($password == $passwordRepeat) {
                if (dbUpdateUser($id, $mail, $password, $username)) {
                    if ($role == 2) {
                        if (isset($post["select-teachers"])) {
                            if (!dbRemoveGroup($id)) {
                                $messageError = "Не удалось удалить ученика у учителя";
                            }
                            if (!dbCreateGroup($post["select-teachers"], $id)) {
                                $messageError = "Не удалось прикрепить ученика к преподавателю";
                            }
                        }
                    }
                    $_SESSION['user'] =  dbFindUser($mail, $password);
                    $isAuth = true;
                    $messageSuccess = "Профиль обновлен.";
                } else {
                    $messageError = dbLastError();
                }
            } else {
                $messageError = "Пароли не совпадают";
            }
        } else {
        }
    } else {
        $messageError = "Не выполнен вход в систему";
    }
}


/**
 * Проверка пользователя на роль учителя 
 */
function hasTeacher()
{
    session_start();
    if ($_SESSION['user']['role'] == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * Проверка ответа
 */
function checkResultTask($id)
{
    $answer = getAnswer($id);
    $task = dbGetTask($answer['task_id']);
    if ($answer['h'] == $task['answer_h'] && $answer['a'] == $task['answer_a'] && $answer['v'] == $task['answer_v']) {
        return true;
    } else {
        return false;
    }
}


/**
 * Получить следующее задание
 */
function getNextTask($task_id)
{
    $tasks = dbGetTasks();

    if (count($tasks) > 1) {
        foreach ($tasks as $key => $task) {
            if ($task['id'] > $task_id) {
                return $task;
            }
        }
        return $tasks[0];
    } else {
        return false;
    }
}

/**
 * База данных. Получить всех преподавателей
 */
function getTeachers()
{
    $users = [];
    foreach (dbGetUsers(1) as $user) {
        $users[] = $user;
    }
    return $users;
}



/**
 * База данных. Получить индивидуального преподавателя по идентификатору ученика
 */
function getIndividualTeachers($child_id)
{
    $groups = dbGetGroupsForChildId($child_id);
    $users = [];
    foreach ($groups as $g) {
        $user = dbGetUserForId($g["parent_id"]);
        $users[] = $user;
    }



    return $users;
}
