<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center align-items-center mt-3 nav-for-mobile">
                        <?php include "header-nav.php" ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Веб - приложение</h4>
                    <p class="text-muted">Веб - приложений является программное приложение, которое работает на веб - сервере, в отличие от компьютера на основе программного обеспечения программ, которые хранятся локально на операционной системе (ОС) устройства.</p>
                </div>
                <div class="col-sm-4 offset-md-1 py-4">
                    <?php if ($isAuth) : ?>
                        <div class="row">
                            <div class="col-12 col-lg-6 d-flex justify-content-center">
                                <h4 class="text-white">Профиль</h4>
                            </div>
                            <div class="col-12 col-lg-6 d-flex justify-content-center">
                                <a href="/?r=profile-update" class="btn btn-sm btn-warning">Редактировать</a>
                            </div>
                        </div>
                        <div class="col-12">
                            <ul class="list-unstyled">
                                <li class="text-light"><b><?= $_SESSION['user']['username'] ?></b> (<?= $_SESSION['user']['id'] ?>)</li>
                                <li class="text-light"><?= dbGetNameRole($_SESSION['user']['role']) ?></li>
                                <li class="text-light"><i><?= $_SESSION['user']['mail'] ?></i></li>
                                <?php if (!hasTeacher()) : ?>
                                    <?php foreach (getIndividualTeachers($_SESSION['user']['id']) as $key => $user) : ?>
                                        <li class="text-light">Ваш преподаватель №<?= $key + 1 ?>: <span class="ml-1"><?= $user['username'] ?></span></li>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </ul>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <div class="row flex-grow-1">
                <div class="col-12 col-md-4">
                    <a href="/" class="navbar-brand d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" aria-hidden="true" class="mr-2" viewBox="0 0 24 24" focusable="false">
                            <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
                            <circle cx="12" cy="13" r="4" />
                        </svg>
                        <strong>«Пирамида» WebGL</strong>
                    </a>

                </div>
                <div class="col-12 col-md-8 d-flex justify-content-end align-items-center">
                    <?php if ($isAuth) : ?>
                        <div class="d-flex justify-content-end align-items-center nav-for-laptop">
                            <?php include "header-nav.php" ?>
                        </div>
                    <?php endif ?>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>

        </div>
    </div>
</header>