<?php
error_reporting(1);


?>
<?php include_once 'db.php' ?>
<?php include_once 'functions.php' ?>
<!DOCTYPE html>

<html lang="en">
<?php include_once 'head.php'; ?>

<body>
    <?php
    include_once 'header.php';

    include_once 'content.php';
    include_once 'footer.php';
    ?>
    <script src="assets/js/jquery-3.5.1.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>

</html>